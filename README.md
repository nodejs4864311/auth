# auth

## 目录介绍
<!-- - `client`: 用户在Application发起授权申请
- `server`: Application服务端
- `oauthClient`: 第三方授权页面
- `oauthService`: 第三方授权服务 -->

## 特别说明
1. 用cookie模拟数据库

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm --filter client run serve
pnpm --filter server run dev
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
pnpm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
