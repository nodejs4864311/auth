import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1684339848153_7382';

  // add your egg config in here
  config.middleware = [];

  config.security = {
    csrf: {
      enable: false,
    },
    // domainWhiteList: [ '*' ],
  };
  // https://juejin.cn/post/6844903897941671949
  config.cors = {
    credentials: true,
    origin: 'http://127.0.0.1:7073',
  };

  // config.cookies = {
  //   maxAge: 1000 * 60 * 60 * 24, // 设置 cookie 过期时间为 1 天
  //   httpOnly: false, // 禁止客户端 JavaScript 访问 cookie
  //   sameSite: 'strict', // 设置 SameSite 属性，防止 CSRF 攻击
  //   signed: false, // 对 cookie 进行签名以防篡改
  //   overwrite: true,
  // };

  // the return config will combines to EggAppConfig
  config.jwtSecret = 'ClientCredentialsRefreshToken';

  return {
    ...config,
  };
};
