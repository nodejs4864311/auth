import crypto from 'crypto';
import { v4 as uuidv4 } from 'uuid';
import base64url from 'base64url';

// 生成随机字符串
function generateRandomString(length) {
  return crypto.randomBytes(Math.ceil(length / 2))
    .toString('hex')
    .slice(0, length);
}

// 根据指定的算法对字符串进行哈希运算
function hashString(algorithm, value) {
  return crypto.createHash(algorithm).update(value).digest('base64');
}

// 创建 PKCE Code Challenge
function createPKCECodeChallenge(codeVerifier) {
  const codeChallenge = base64url.encode(hashString('sha256', codeVerifier));
  return codeChallenge;
}

export default {
  // 生成随机数
  generateAndSaveState(): string {
    const state = generateRandomString(64);
    return state;
  },
  // 生成访问令牌
  generateAccessToken(): string {
    const token = uuidv4();
    return token;
  },
  // 生成 code_verifier 和 code_challenge
  generatePKCECodes() {
    const verifier = generateRandomString(64);
    const challenge = createPKCECodeChallenge(verifier);
    return {
      verifier,
      challenge,
    };
  },
  verifyPKCECode(codeVerifier, codeChallenge, method) {
    let hashedCodeVerifier;
    if (method === 'plain') {
      hashedCodeVerifier = codeVerifier;
    } else if (method === 'S256') {
      hashedCodeVerifier = base64url.encode(hashString('sha256', codeVerifier));
    } else {
      throw new Error(`Unsupported PKCE code challenge method: ${method}`);
    }
    return hashedCodeVerifier === codeChallenge;
  },
};

