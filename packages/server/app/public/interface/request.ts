export interface CLIENTS_RESPONSE {
  clientId: string;
  authEnpoint: string;
  state: string;
}

export interface CODE_CHALLENGE_CLIENTS_RESPONSE {
  clientId: string;
  authEnpoint: string;
  state: string;
  challenge: string;
}

export interface CLIENTS {
  clientId: string;
  authEnpoint: string;
  clientSecret: string;
}

export interface CLIENT_CREDENTIALS {
  client_id: string;
  client_secret: string;
}

export interface TOKEN {
  code: string;
  clientId: string;
  clientSecret: string;
}

export interface CLIENT_CREDENTIALS_TOKEN {
  client_id: string;
  client_secret: string;
  grant_type: string;
}

export interface REFRESH_TOKEN {
  client_id: string;
  client_secret: string;
  grant_type: string;
  refresh_token: string;
}

export interface USER {
  username: string;
  password: string;
}

export interface CODE {
  verifier: string;
  challenge: string;
}

export interface OAUTH_CODE_CHALLENGE {
  challenge: string;
  challenge_method: string;
}

export interface PKCE_TOKEN {
  code: string;
  clientId: string;
  clientSecret: string;
  codeVerifier: string;
}
