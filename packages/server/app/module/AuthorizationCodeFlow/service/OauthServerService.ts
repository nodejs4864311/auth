// 特别声明，这里只是简单模拟OAuth的流程，实际缺少很多复杂的校验，且不知道流程处理是否正确
import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { USER, TOKEN, CLIENTS } from '@/public/interface/request';

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class OauthServerService {
  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  async getRegisterClient(ctx: EggContext): Promise<any> {
    const clients:CLIENTS = {
      clientId: 'tpaCmJBYxOJ8BtM2Dbbnxh5a',
      clientSecret: 'rgUFrrmkyNSztL63h8wbTQQZUa-BeEc3Yi-CECTBeGAR1PWZ',
      authEnpoint: 'http://127.0.0.1:7073/authorize',
    };

    ctx.cookies.set('clients', JSON.stringify(clients), { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });

    return clients;
  }

  // 授权服务器检查用户授权状态
  async getAuthorizeStatus(ctx: EggContext): Promise<any> {
    // 判断用户是否已经登录(在有效期内)
    const { username, password } = JSON.parse(ctx.cookies.get('user', { signed: false }) || '{}');
    console.log('userrrrrr:', username, password);
    if (!username || !password) {
      return {
        code: -1,
        data: null,
        message: '用户未登录',
      };
    }

    const { client_id } = ctx.request.query || {};
    console.log('queryyyy:', ctx.request.query);
    const oauthServerClients = JSON.parse(ctx.cookies.get('clients', { signed: false }) || '{}');
    console.log('oauthserverclients:', oauthServerClients, client_id, oauthServerClients.clientId);
    // 校验是否是合法的Application请求
    if (client_id !== oauthServerClients.clientId) {
      return {
        code: -1,
        data: null,
        message: 'Application唯一标识不匹配',
      };
    }

    if (username === 'demo@example.com' && password === 'helloworld123') {
      return {
        code: 0,
        data: '',
        message: '用户已授权',
      };
    }

    return {
      code: -2,
      data: null,
      message: '服务异常',
    };
  }

  // 将写死的用户名和密码存在cookie中，非常简单模拟登录...
  // 用户账号：demo@example.com
  // 密码：helloworld123
  async login(ctx: EggContext): Promise<any> {
    const { username, password } = ctx.request.body;
    // 只简单校验是否有值，校验、加密逻辑都忽略，不是这次学习重点...
    if (username === 'demo@example.com' && password === 'helloworld123') {
      const user:USER = {
        username: 'demo@example.com',
        password: 'helloworld123',
      };

      // 设置2个小时的过期时间
      const now = new Date();
      const twoHoursLater = new Date(now.getTime() + (2 * 60 * 1000));
      ctx.cookies.set('user', JSON.stringify(user), { expires: twoHoursLater, signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });

      return {
        code: 0,
        data: {},
        message: '登录成功',
      };
    }
  }

  async approve(ctx: EggContext): Promise<any> {
    try {
      const { clientId } = ctx.request.query || {};
      const oauthServerClients = JSON.parse(ctx.cookies.get('clients', { signed: false }) || '{}');

      // 校验是否是合法的Application请求
      if (clientId !== oauthServerClients.clientId) {
        throw {
          code: -1,
          data: null,
          message: 'Application唯一标识不匹配',
        };
      }

      const token = {
        code: ctx.generateAccessToken(),
      };
      ctx.cookies.set('token', JSON.stringify(token), { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });
      return {
        code: 0,
        data: {
          code: token.code,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }
  // 用code换取accessToken
  async token(ctx: EggContext, params:TOKEN): Promise<any> {
    try {
      const { clientId, clientSecret, code } = params;
      const oauthServerClients = JSON.parse(ctx.cookies.get('clients', { signed: false }) || '{}');
      const oauthServerCode = JSON.parse(ctx.cookies.get('token', { signed: false }) || '{}');

      if (clientId !== oauthServerClients.clientId) {
        return {
          code: -1,
          data: null,
          message: '用户ID不匹配',
        };
      }

      if (clientSecret !== oauthServerClients.clientSecret) {
        return {
          code: -1,
          data: null,
          message: '用户密钥不匹配',
        };
      }

      if (code !== oauthServerCode.code) {
        return {
          code: -1,
          data: null,
          message: 'code不匹配，无法交换assessToken',
        };
      }

      // 验证code和clientId OK后，生成access token
      const accessToken = ctx.generateAccessToken();
      return accessToken;
    } catch (error) {
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }
}

