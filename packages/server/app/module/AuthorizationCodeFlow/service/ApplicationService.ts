import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { CLIENTS, CLIENTS_RESPONSE, TOKEN } from '@/public/interface/request';
import { OauthServerService } from '@/module/AuthorizationCodeFlow';

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class ApplicationService {
  // 实际应该请求到OauthServer, 用Inject模拟从后端请求到OauthServer
  @Inject()
  OauthServerService: OauthServerService;

  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  async getRegisterClient(ctx: EggContext): Promise<any> {
    // 模拟请求oauth server 获取用户配置信息接口
    try {
      const clientsRes:CLIENTS = await this.OauthServerService.getRegisterClient(ctx);
      const { clientId, clientSecret, authEnpoint } = clientsRes;
      ctx.session.state = ctx.generateAndSaveState();
      const state = ctx.session.state;

      const clients = {
        clientId,
        clientSecret,
      };

      ctx.cookies.set('applicationClients', JSON.stringify(clients), { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });

      return {
        code: 0,
        data: { clientId, state, authEnpoint } as CLIENTS_RESPONSE,
        message: '获取数据成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }

  async exchangAccessToken(ctx: EggContext): Promise<any> {
    try {
      const body = ctx.request.body;
      const applicationClients = ctx.cookies.get('applicationClients', { signed: false });

      if (!applicationClients) {
        return {
          code: -1,
          data: null,
          message: '缺少应用程序客户端信息',
        };
      }
      const { clientId, clientSecret } = JSON.parse(applicationClients);
      if (body.state !== ctx.session.state) {
        return {
          code: -1,
          data: null,
          message: 'state不匹配，请重新授权',
        };
      }

      const params:TOKEN = {
        clientId,
        clientSecret,
        code: body.code,
      };
      // 模拟请求oauth server换取assessToken接口
      const getAccessToken = await this.OauthServerService.token(ctx, params);

      // 请求成功之后，state清空，确保state时效性
      ctx.session.state = null;

      return {
        code: 0,
        data: getAccessToken,
        message: '换取access token成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '无效的应用程序客户端信息',
      };
    }
  }

  async clearCookies(ctx: EggContext): Promise<any> {
    try {
      ctx.cookies.set('applicationClients', null, { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });
      ctx.cookies.set('clients', null, { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });
      ctx.cookies.set('token', null, { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });
      ctx.cookies.set('user', null, { signed: false, httpOnly: false, maxAge: 1000 * 60 * 60 * 24 });

      return {
        code: 0,
        data: null,
        message: '清除cookie成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '无效的应用程序客户端信息',
      };
    }
  }
}
