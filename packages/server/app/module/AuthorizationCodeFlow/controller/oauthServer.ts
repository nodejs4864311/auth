import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext } from '@eggjs/tegg';
import { OauthServerService } from '@/module/AuthorizationCodeFlow';
import { EggLogger } from 'egg';

@HTTPController({
  path: '/api',
})
export class OauthServerController {
  @Inject()
  OauthServerService: OauthServerService;

  @Inject()
  logger: EggLogger;

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/authorize',
  })

  async index(@Context() ctx: EggContext) {
    return await this.OauthServerService.getAuthorizeStatus(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/login',
  })

  async login(@Context() ctx: EggContext) {
    return await this.OauthServerService.login(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/approve',
  })

  async approve(@Context() ctx: EggContext) {
    return await this.OauthServerService.approve(ctx);
  }
}
