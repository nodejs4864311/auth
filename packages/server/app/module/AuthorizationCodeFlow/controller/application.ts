import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext } from '@eggjs/tegg';
import { ApplicationService } from '@/module/AuthorizationCodeFlow';
import { EggLogger } from 'egg';

@HTTPController({
  path: '/api',
})
export class ApplicationController {
  @Inject()
  ApplicationService: ApplicationService;

  @Inject()
  logger: EggLogger;

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/getRegisterClient',
  })

  async index(@Context() ctx: EggContext) {
    return await this.ApplicationService.getRegisterClient(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/exchangeAccesstoken',
  })

  async exchangeToken(@Context() ctx: EggContext) {
    return await this.ApplicationService.exchangAccessToken(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/clearCookies',
  })

  async clear(@Context() ctx: EggContext) {
    return await this.ApplicationService.clearCookies(ctx);
  }
}
