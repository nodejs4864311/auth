import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext } from '@eggjs/tegg';
import { ApplicationCredentialsService } from '@/module/ClientCredentials';
import { EggLogger } from 'egg';

@HTTPController({
  path: '/credentialsapi',
})
export class ApplicationCredentialsController {
  @Inject()
  ApplicationCredentialsService: ApplicationCredentialsService;

  @Inject()
  logger: EggLogger;

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/getToken',
  })

  async index(@Context() ctx: EggContext) {
    return await this.ApplicationCredentialsService.getToken(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/refreshToken',
  })

  async refresh(@Context() ctx: EggContext) {
    return await this.ApplicationCredentialsService.refresh(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/getExpireInStatus',
  })

  async check(@Context() ctx: EggContext) {
    return await this.ApplicationCredentialsService.checkAccessToken(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/clearCookies',
  })

  async clear(@Context() ctx: EggContext) {
    return await this.ApplicationCredentialsService.clearCookies(ctx);
  }
}
