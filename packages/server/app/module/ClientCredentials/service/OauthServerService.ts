// 特别声明，这里只是简单模拟OAuth的流程，实际缺少很多复杂的校验，且不知道流程处理是否正确
import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { CLIENT_CREDENTIALS_TOKEN, CLIENT_CREDENTIALS, REFRESH_TOKEN } from '@/public/interface/request';
import jwt from 'jsonwebtoken';

const clientsStore:CLIENT_CREDENTIALS = {
  client_id: 'tpaCmJBYxOJ8BtM2Dbbnxh5a',
  client_secret: 'rgUFrrmkyNSztL63h8wbTQQZUa-BeEc3Yi-CECTBeGAR1PWZ',
};

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class OauthServerCredentialsService {
  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  @Inject()
  config: {
    jwtSecret: string;
  };

  async getClientCredentials(): Promise<any> {
    return clientsStore;
  }

  // 检查访问令牌是否有效
  async checkAccessToken(ctx:EggContext) {
    const accessToken = ctx.cookies.get('access_token', { signed: false });
    if (!accessToken) {
      // 访问令牌不存在，说明已过期
      return false;
    }

    // 检查访问令牌是否有效
    try {
      jwt.verify(accessToken, this.config.jwtSecret);
    } catch (error) {
      return false;
    }

    // 访问令牌有效
    return true;
  }

  // 检查刷新令牌是否有效
  async checkRefreshToken(ctx:EggContext) {
    const refreshToken = ctx.cookies.get('refresh_token', { signed: false });
    if (!refreshToken) {
      // 刷新令牌不存在，说明已过期
      return false;
    }

    // 刷新令牌有效
    return true;
  }

  // 使用client credentials获取token
  async token(ctx: EggContext, params:CLIENT_CREDENTIALS_TOKEN): Promise<any> {
    try {
      const { client_id, client_secret, grant_type } = params;

      if (grant_type !== 'client_credentials') {
        return {
          code: -1,
          data: null,
          message: '模式不对',
        };
      }

      if (client_id !== clientsStore.client_id) {
        return {
          code: -1,
          data: null,
          message: '用户ID不匹配',
        };
      }

      if (client_secret !== clientsStore.client_secret) {
        return {
          code: -1,
          data: null,
          message: '用户密钥不匹配',
        };
      }

      // 颁发访问令牌(Access Token)和刷新令牌(Refresh Token)
      // Access Token 短期有效凭据
      // Refresh Token 长期有效凭据
      const accessToken = jwt.sign({ token: clientsStore.client_id, secret: clientsStore.client_secret }, this.config.jwtSecret, { expiresIn: '5m' });
      const refreshToken = jwt.sign({ token: clientsStore.client_id }, this.config.jwtSecret, { expiresIn: '1h' });

      // 将访问令牌(Access Token)和刷新令牌(Refresh Token)存Cookie中
      ctx.cookies.set('access_token', accessToken, { httpOnly: false, signed: false });
      ctx.cookies.set('refresh_token', refreshToken, { httpOnly: false, signed: false });

      return {
        code: 0,
        data: {
          accessToken,
          refreshToken,
          expiresIn: 3600,
        },
        message: '成功生成 token',
      };
    } catch (error) {
      console.log('get token error:', error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }

  // refresh token
  async refresh(ctx: EggContext, params:REFRESH_TOKEN): Promise<any> {
    const { client_id, client_secret, grant_type, refresh_token } = params;

    if (grant_type !== 'client_credentials') {
      return {
        code: -1,
        data: null,
        message: '模式不对',
      };
    }

    if (client_id !== clientsStore.client_id) {
      return {
        code: -1,
        data: null,
        message: '用户ID不匹配',
      };
    }

    if (client_secret !== clientsStore.client_secret) {
      return {
        code: -1,
        data: null,
        message: '用户密钥不匹配',
      };
    }

    // 检查刷新令牌是否有效
    try {
      jwt.verify(refresh_token, this.config.jwtSecret);
    } catch (error) {
      return {
        code: -1,
        data: null,
        message: 'refresh token过期了!',
      };
    }

    // 颁发访问令牌(Access Token)和刷新令牌(Refresh Token)
    // Access Token 短期有效凭据
    // Refresh Token 长期有效凭据
    const accessToken = jwt.sign({ token: clientsStore.client_id, secret: clientsStore.client_secret }, this.config.jwtSecret, { expiresIn: '5m' });
    const refreshToken = jwt.sign({ token: clientsStore.client_id }, this.config.jwtSecret, { expiresIn: '1h' });

    // 将访问令牌(Access Token)和刷新令牌(Refresh Token)存Cookie中
    ctx.cookies.set('access_token', accessToken, { httpOnly: false, signed: false });
    ctx.cookies.set('refresh_token', refreshToken, { httpOnly: false, signed: false });

    return {
      code: 0,
      data: {
        // 如果访问令牌将过期，则返回刷新令牌很有用
        // 应用程序可以使用该刷新令牌来获取另一个访问令牌
        refreshToken,
        accessToken,
        expiresIn: 3600,
      },
      message: '成功生成 token',
    };
  }
}

