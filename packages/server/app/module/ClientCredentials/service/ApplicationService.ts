import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { CLIENT_CREDENTIALS } from '@/public/interface/request';
import { OauthServerCredentialsService } from '@/module/ClientCredentials';

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class ApplicationCredentialsService {
  // 实际应该请求到OauthServer, 用Inject模拟从后端请求到OauthServer
  @Inject()
  OauthServerCredentialsService: OauthServerCredentialsService;

  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  async getToken(ctx: EggContext): Promise<any> {
    try {
      // 模拟请求oauth server 获取用户配置信息接口
      const clientsRes:CLIENT_CREDENTIALS = await this.OauthServerCredentialsService.getClientCredentials();
      const { client_id, client_secret } = clientsRes;

      // 通过client id和client secret获取token
      const access = await this.OauthServerCredentialsService.token(ctx, {
        client_id,
        client_secret,
        grant_type: 'client_credentials',
      });
      if (access.code !== 0) {
        throw access.message;
      }

      const { accessToken, expiresIn, refreshToken } = access.data;

      return {
        code: 0,
        data: {
          accessToken,
          refreshToken,
          expiresIn,
        },
        message: '成功获取token',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }

  async refresh(ctx: EggContext): Promise<any> {
    const { refreshToken } = ctx.request.body;
    // 模拟请求oauth server 获取用户配置信息接口
    const clientsRes:CLIENT_CREDENTIALS = await this.OauthServerCredentialsService.getClientCredentials();
    const { client_id, client_secret } = clientsRes;

    const refresh = await this.OauthServerCredentialsService.refresh(ctx, {
      client_id,
      client_secret,
      grant_type: 'client_credentials',
      refresh_token: refreshToken,
    });
    if (refresh.code !== 0) {
      throw refresh.message;
    }

    const { accessToken, expiresIn, refreshToken: refreshID } = refresh.data;

    return {
      code: 0,
      data: {
        accessToken,
        refreshToken: refreshID,
        expiresIn,
      },
      message: '成功获取token',
    };
  }

  async checkAccessToken(ctx: EggContext): Promise<any> {
    const checkAccessToken = await this.OauthServerCredentialsService.checkAccessToken(ctx);

    if (!checkAccessToken) {
      return {
        code: 401,
        data: null,
        message: 'access token过期了!',
      };
    }

    return {
      code: 0,
      data: null,
      message: 'token未过期',
    };
  }

  async clearCookies(ctx: EggContext): Promise<any> {
    try {
      ctx.cookies.set('access_token', '', { signed: false, httpOnly: false });
      ctx.cookies.set('refresh_token', '', { signed: false, httpOnly: false });

      return {
        code: 0,
        data: null,
        message: '清除cookie成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '无效的应用程序客户端信息',
      };
    }
  }
}
