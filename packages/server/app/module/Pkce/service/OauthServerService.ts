// 特别声明，这里只是简单模拟OAuth的流程，实际缺少很多复杂的校验，且不知道流程处理是否正确
import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { USER, PKCE_TOKEN, CLIENTS, OAUTH_CODE_CHALLENGE } from '@/public/interface/request';

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class OauthServerPkceService {
  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  async getRegisterClient(ctx: EggContext): Promise<any> {
    const clients:CLIENTS = {
      clientId: 'tpaCmJBYxOJ8BtM2Dbbnxh5a',
      clientSecret: 'rgUFrrmkyNSztL63h8wbTQQZUa-BeEc3Yi-CECTBeGAR1PWZ',
      authEnpoint: 'http://127.0.0.1:7073/pkce-authorize',
    };

    ctx.cookies.set('clientsPkce', JSON.stringify(clients));

    return clients;
  }

  // 授权服务器检查用户授权状态
  async getAuthorizeStatus(ctx: EggContext): Promise<any> {
    const { client_id, code_challenge, code_challenge_method } = ctx.request.query || {};
    const { username, password } = JSON.parse(ctx.cookies.get('userPkce', { signed: false }) || '{}');
    const oauthServerClients = JSON.parse(ctx.cookies.get('clientsPkce', { signed: false }) || '{}');

    // 校验是否是合法的Application请求
    if (client_id !== oauthServerClients.clientId) {
      return {
        code: -1,
        data: null,
        message: 'Application唯一标识不匹配',
      };
    }

    if (!username || !password) {
      return {
        code: -1,
        data: null,
        message: '用户未登录',
      };
    }

    if (username === 'demo@example.com' && password === 'helloworld123') {
      // 先将code challenge存起来，用于交换授权码时使用
      const code:OAUTH_CODE_CHALLENGE = {
        challenge: code_challenge,
        challenge_method: code_challenge_method,
      };
      ctx.cookies.set('OauthServerChallenge', JSON.stringify(code), { signed: false });

      return {
        code: 0,
        data: '',
        message: '用户已授权',
      };
    }

    return {
      code: -2,
      data: null,
      message: '服务异常',
    };
  }

  // 将写死的用户名和密码存在cookie中，非常简单模拟登录...
  // 用户账号：demo@example.com
  // 密码：helloworld123
  async login(ctx: EggContext): Promise<any> {
    const { username, password, client_id, code_challenge, code_challenge_method } = ctx.request.body;
    const oauthServerClients = JSON.parse(ctx.cookies.get('clientsPkce', { signed: false }) || '{}');

    // 校验是否是合法的Application请求
    if (client_id !== oauthServerClients.clientId) {
      return {
        code: -1,
        data: null,
        message: 'Application唯一标识不匹配',
      };
    }

    // 只简单校验是否有值，校验、加密逻辑都忽略，不是这次学习重点...
    if (username === 'demo@example.com' && password === 'helloworld123') {
      const user:USER = {
        username: 'demo@example.com',
        password: 'helloworld123',
      };

      // 设置2个小时的过期时间
      const now = new Date();
      const twoHoursLater = new Date(now.getTime() + (2 * 60 * 1000));
      ctx.cookies.set('userPkce', JSON.stringify(user), { expires: twoHoursLater });

      // 先将code challenge存起来，用于交换授权码时使用
      const code:OAUTH_CODE_CHALLENGE = {
        challenge: code_challenge,
        challenge_method: code_challenge_method, // 这里例子用不到这个参数，因为写死了sha256算法
      };
      ctx.cookies.set('OauthServerChallenge', JSON.stringify(code), { signed: false });

      return {
        code: 0,
        data: {},
        message: '登录成功',
      };
    }
  }

  async approve(ctx: EggContext): Promise<any> {
    try {
      const { clientId } = ctx.request.query || {};
      const oauthServerClients = JSON.parse(ctx.cookies.get('clientsPkce', { signed: false }) || '{}');

      // 校验是否是合法的Application请求
      if (clientId !== oauthServerClients.clientId) {
        return {
          code: -1,
          data: null,
          message: 'Application唯一标识不匹配',
        };
      }

      const token = {
        code: ctx.generateAccessToken(),
      };
      ctx.cookies.set('tokenPkce', JSON.stringify(token));
      return {
        code: 0,
        data: {
          code: token.code,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }
  // 用code换取accessToken
  async token(ctx: EggContext, params:PKCE_TOKEN): Promise<any> {
    try {
      const { clientId, clientSecret, code, codeVerifier } = params;
      const oauthServerClients = JSON.parse(ctx.cookies.get('clientsPkce', { signed: false }) || '{}');
      const oauthServerCode = JSON.parse(ctx.cookies.get('tokenPkce', { signed: false }) || '{}');

      if (clientId !== oauthServerClients.clientId) {
        return {
          code: -1,
          data: null,
          message: '用户ID不匹配',
        };
      }

      if (clientSecret !== oauthServerClients.clientSecret) {
        return {
          code: -1,
          data: null,
          message: '用户密钥不匹配',
        };
      }

      if (code !== oauthServerCode.code) {
        return {
          code: -1,
          data: null,
          message: 'code不匹配，无法交换assessToken',
        };
      }

      // pkce：将codeChallenge解出codeVerifier是否与“获取用户授权”接口传过来的codeChallenge相等
      // 此处只是简单的处理，实际OAuth Server比这里处理复杂非常多
      const { challenge, challenge_method } = JSON.parse(ctx.cookies.get('OauthServerChallenge', { signed: false }) || '{}');
      const verifyPKCECode = ctx.verifyPKCECode(codeVerifier, challenge, challenge_method);

      if (!verifyPKCECode) {
        return {
          code: -1,
          data: null,
          message: '非法获取access token',
        };
      }

      // 验证code和clientId OK后，生成access token
      return {
        code: 0,
        data: ctx.generateAccessToken(),
        message: '获取access token成功',
      };

    } catch (error) {
      console.log(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }
}

