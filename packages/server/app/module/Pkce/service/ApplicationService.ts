import { EggLogger } from 'egg';
import { SingletonProto, AccessLevel, Inject, EggContext } from '@eggjs/tegg';
import { CLIENTS, CODE_CHALLENGE_CLIENTS_RESPONSE, PKCE_TOKEN, CODE } from '@/public/interface/request';
import { OauthServerPkceService } from '@/module/Pkce';

@SingletonProto({
  // 如果需要在上层使用，需要把 accessLevel 显示声明为 public
  accessLevel: AccessLevel.PUBLIC,
})
export class ApplicationPkceService {
  // 实际应该请求到OauthServer, 用Inject模拟从后端请求到OauthServer
  @Inject()
  OauthServerPkceService: OauthServerPkceService;

  // 注入一个 logger
  @Inject()
  logger: EggLogger;

  async generateCodeChallenge(ctx: EggContext): Promise<any> {
    try {
      const code:CODE = ctx.generatePKCECodes();
      ctx.cookies.set('codeChallenge', JSON.stringify(code), { signed: false });

      return {
        code: 0,
        data: code,
        message: '生成code challenge成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }

  async getRegisterClient(ctx: EggContext): Promise<any> {
    // 模拟请求oauth server 获取用户配置信息接口
    try {
      const clientsRes:CLIENTS = await this.OauthServerPkceService.getRegisterClient(ctx);
      const { clientId, clientSecret, authEnpoint } = clientsRes;
      const codeChallengeCookies = ctx.cookies.get('codeChallenge', { signed: false }) || '{}';
      const { challenge } = JSON.parse(codeChallengeCookies);
      ctx.session.state = ctx.generateAndSaveState();
      const state = ctx.session.state;

      const clients = {
        clientId,
        clientSecret,
      };

      ctx.cookies.set('applicationClientsPkce', JSON.stringify(clients));

      return { clientId, state, authEnpoint, challenge } as CODE_CHALLENGE_CLIENTS_RESPONSE;
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '获取应用程序客户端信息失败',
      };
    }
  }

  // 用code去OAuth Server交换access token
  async exchangAccessToken(ctx: EggContext): Promise<any> {
    try {
      const body = ctx.request.body;
      const applicationClients = ctx.cookies.get('applicationClientsPkce', { signed: false });

      if (!applicationClients) {
        return {
          code: -1,
          data: null,
          message: '缺少应用程序客户端信息',
        };
      }
      const { clientId, clientSecret } = JSON.parse(applicationClients);
      if (body.state !== ctx.session.state) {
        return {
          code: -1,
          data: null,
          message: 'state不匹配，请重新授权',
        };
      }

      // 用Authorization Code换取Access Token，需要传code challenge
      const codeChallengeCookies = ctx.cookies.get('codeChallenge', { signed: false });
      const { verifier } = JSON.parse(codeChallengeCookies || '{}');

      const params:PKCE_TOKEN = {
        clientId,
        clientSecret,
        code: body.code,
        codeVerifier: verifier,
      };
      // 模拟请求oauth server换取assessToken接口
      const { code, data: getAccessToken, message } = await this.OauthServerPkceService.token(ctx, params);
      console.log('codeeeee:', code, getAccessToken, message);
      if (code !== 0) {
        return {
          code,
          data: null,
          message,
        };
      }

      // 请求成功之后，state清空，确保state时效性
      ctx.session.state = null;

      return {
        code: 0,
        data: getAccessToken,
        message: '换取access token成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '无效的应用程序客户端信息',
      };
    }
  }

  async clearCookies(ctx: EggContext): Promise<any> {
    try {
      ctx.cookies.set('applicationClientsPkce', null);
      ctx.cookies.set('clientsPkce', null);
      ctx.cookies.set('tokenPkce', null);
      ctx.cookies.set('userPkce', null);
      ctx.cookies.set('OauthServerChallenge', null);

      return {
        code: 0,
        data: null,
        message: '清除cookie成功',
      };
    } catch (error) {
      console.error(error);
      return {
        code: -1,
        data: null,
        message: '无效的应用程序客户端信息',
      };
    }
  }
}
