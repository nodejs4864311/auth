import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext } from '@eggjs/tegg';
import { ApplicationPkceService } from '@/module/Pkce';
import { EggLogger } from 'egg';

@HTTPController({
  path: '/pkceapi',
})
export class ApplicationPkceController {
  @Inject()
  ApplicationPkceService: ApplicationPkceService;

  @Inject()
  logger: EggLogger;

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/generateCodeChallenge',
  })

  async generate(@Context() ctx: EggContext) {
    return await this.ApplicationPkceService.generateCodeChallenge(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/getRegisterClient',
  })

  async index(@Context() ctx: EggContext) {
    return await this.ApplicationPkceService.getRegisterClient(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/exchangeAccesstoken',
  })

  async exchangeToken(@Context() ctx: EggContext) {
    return await this.ApplicationPkceService.exchangAccessToken(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/clearCookies',
  })

  async clear(@Context() ctx: EggContext) {
    return await this.ApplicationPkceService.clearCookies(ctx);
  }
}
