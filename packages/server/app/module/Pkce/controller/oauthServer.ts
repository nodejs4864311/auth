import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext } from '@eggjs/tegg';
import { OauthServerPkceService } from '@/module/Pkce';
import { EggLogger } from 'egg';

@HTTPController({
  path: '/pkceapi',
})
export class OauthServerPkceController {
  @Inject()
  OauthServerPkceService: OauthServerPkceService;

  @Inject()
  logger: EggLogger;

  // 获取用户权限
  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/authorize',
  })

  async index(@Context() ctx: EggContext) {
    return await this.OauthServerPkceService.getAuthorizeStatus(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.POST,
    path: '/login',
  })

  async login(@Context() ctx: EggContext) {
    return await this.OauthServerPkceService.login(ctx);
  }

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/approve',
  })

  async approve(@Context() ctx: EggContext) {
    return await this.OauthServerPkceService.approve(ctx);
  }
}
