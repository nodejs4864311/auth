import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/pkce-generate",
    name: "pkce-generate",
    component: () =>
      import(
        /* webpackChunkName: "pkce-generate" */ "../../components/pkce/application/Generate.vue"
      ),
  },
  {
    path: "/pkce-apply",
    name: "pkce-apply",
    component: () =>
      import(
        /* webpackChunkName: "pkce-apply" */ "../../components/pkce/application/Apply.vue"
      ),
  },
  {
    path: "/pkce-success",
    name: "pkce-success",
    component: () =>
      import(
        /* webpackChunkName: "pkce-success" */ "../../components/pkce/application/Success.vue"
      ),
  },
  {
    path: "/pkce-authorize",
    name: "pkce-authorize",
    component: () =>
      import(
        /* webpackChunkName: "pkce-authorize" */ "../../components/pkce/oauthServer/Authorize.vue"
      ),
  },
  {
    path: "/pkce-login",
    name: "pkce-login",
    component: () =>
      import(
        /* webpackChunkName: "pkce-login" */ "../../components/pkce/oauthServer/Login.vue"
      ),
  },
  {
    path: "/pkce-approve",
    name: "pkce-approve",
    component: () =>
      import(
        /* webpackChunkName: "pkce-approve" */ "../../components/pkce/oauthServer/Approve.vue"
      ),
  },
];

export default routes;
