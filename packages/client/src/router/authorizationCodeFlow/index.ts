import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/apply",
    name: "apply",
    component: () =>
      import(
        /* webpackChunkName: "apply" */ "../../components/authorizationCodeFlow/application/Apply.vue"
      ),
  },
  {
    path: "/success",
    name: "success",
    component: () =>
      import(
        /* webpackChunkName: "success" */ "../../components/authorizationCodeFlow/application/Success.vue"
      ),
  },
  {
    path: "/authorize",
    name: "authorize",
    component: () =>
      import(
        /* webpackChunkName: "authorize" */ "../../components/authorizationCodeFlow/oauthServer/Authorize.vue"
      ),
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(
        /* webpackChunkName: "login" */ "../../components/authorizationCodeFlow/oauthServer/Login.vue"
      ),
  },
  {
    path: "/approve",
    name: "approve",
    component: () =>
      import(
        /* webpackChunkName: "approve" */ "../../components/authorizationCodeFlow/oauthServer/Approve.vue"
      ),
  },
];

export default routes;
