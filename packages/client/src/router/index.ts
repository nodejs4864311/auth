import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import authorizationCodeRouter from "@/router/authorizationCodeFlow/index";
import pkceRouter from "@/router/pkce/index";
import clientCredentialsRouter from "@/router/clientCredentials/index";

const routes: Array<RouteRecordRaw> = [
  ...pkceRouter,
  ...authorizationCodeRouter,
  ...clientCredentialsRouter,
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
