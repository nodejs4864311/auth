import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/credentials-apply",
    name: "credentials-apply",
    component: () =>
      import(
        /* webpackChunkName: "credentials-apply" */ "../../components/clientCredentials/application/Apply.vue"
      ),
  },
  {
    path: "/credentials-success",
    name: "credentials-success",
    component: () =>
      import(
        /* webpackChunkName: "credentials-success" */ "../../components/clientCredentials/application/Success.vue"
      ),
  },
];

export default routes;
