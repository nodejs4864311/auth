import { getCurrentInstance } from "vue";
import axios from "axios";

export function useGlobalProperties() {
  const vm = getCurrentInstance();
  const globalProperties = vm?.appContext.config.globalProperties || null;
  const $http = globalProperties?.$http || axios; // 先简单做，后续优化

  return {
    $http,
  };
}
