export interface FormState {
  username: string;
  password: string;
}

export interface CLIENTS_RESPONSE {
  clientId: string;
  authEnpoint: string;
  state: string;
}

export interface CODE_CHALLENGE_CLIENTS_RESPONSE {
  clientId: string;
  authEnpoint: string;
  state: string;
  challenge: string;
}

export interface CODE {
  verifier: string;
  challenge: string;
}
