import { createApp } from "vue";
import App from "@/App.vue";
import router from "@/router/index";
import {
  Button,
  Layout,
  Form,
  Input,
  Steps,
  Descriptions,
  List,
} from "ant-design-vue";
import axios from "axios";
import { AxiosInstance } from "axios";

const app = createApp(App);

// 将axios挂载全局
const Axios: AxiosInstance = axios;
app.config.globalProperties.$axios = Axios;
declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $http: AxiosInstance;
  }
}

app
  .use(Button)
  .use(Layout)
  .use(Form)
  .use(Input)
  .use(Steps)
  .use(Descriptions)
  .use(List)
  .use(router)
  .mount("#app");
