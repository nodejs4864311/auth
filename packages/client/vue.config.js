const { defineConfig } = require("@vue/cli-service");
const Components = require("unplugin-vue-components/webpack");
const { AntDesignVueResolver } = require("unplugin-vue-components/resolvers");

module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    plugins: [
      Components({
        directoryAsNamespace: true,
        resolvers: [AntDesignVueResolver()],
      }),
    ],
  },
  devServer: {
    port: 7073,
    host: "0.0.0.0",
    proxy: {
      "/api": {
        target: "http://127.0.0.1:7001",
      },
      "/pkceapi": {
        target: "http://127.0.0.1:7001",
      },
      "/credentialsapi": {
        target: "http://127.0.0.1:7001",
      },
    },
  },
});
